import * as React from "react";
import { registerRootComponent } from "expo";
import { Provider as PaperProvider } from "react-native-paper";
import App from "./src/App";

class Main extends React.Component {
  render() {
    return (
      <PaperProvider>
        <App />
      </PaperProvider>
    );
  }
}

registerRootComponent(Main);
