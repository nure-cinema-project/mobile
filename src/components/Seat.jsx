import * as React from "react";
import { TouchableOpacity } from "react-native";
import { Text } from "react-native-paper";

export default function Seat({
  line,
  seat,
  text,
  onPressDone,
  pickColor,
  disabled,
}) {
  const [Pressed, setPressed] = React.useState(false);
  let color = "lightgray";
  if (Pressed) color = pickColor;
  if (disabled) color = "gray";
  return (
    <TouchableOpacity
      style={{
        width: 30,
        alignItems: "center",
        borderRadius: 3,
        backgroundColor: color,
      }}
      onPress={() => {
        const newValue = Pressed ? false : true;
        setPressed(newValue);
        onPressDone(line, seat, newValue);
      }}
      disabled={disabled}
    >
      <Text style={{ lineHeight: 30 }}>{text}</Text>
    </TouchableOpacity>
  );
}
