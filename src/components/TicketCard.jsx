import * as React from "react";
import { Text, View } from "react-native";

export default function TicketCard({ ticket }) {
  return (
    <View
      style={{
        backgroundColor: "lightgrey",
        borderRadius: 15,
        width: "80%",
        marginBottom: "3%",
        marginLeft: "10%",
        paddingVertical: "3%",
        paddingHorizontal: "5%",
      }}
    >
      <Text style={{ fontSize: 30 }}>Ticket #{ticket.number}</Text>
      <Text>Film: {ticket.filmName}</Text>
      <Text>Time: {ticket.time}</Text>
      <Text>Hall: {ticket.hallName}</Text>
      <Text>Line: {ticket.line}</Text>
      <Text>Seat: {ticket.seat}</Text>
      <Text>Price: {ticket.price}</Text>
    </View>
  );
}
