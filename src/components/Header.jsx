import React, { memo } from "react";
import { StyleSheet, Text } from "react-native";
import { theme } from "../styles/theme";

const Header = ({ style, children }) => (
  <Text style={[styles.header, style]}>{children}</Text>
);

const styles = StyleSheet.create({
  header: {
    fontSize: 26,
    color: theme.colors.primary,
    fontWeight: "bold",
    paddingVertical: 14,
  },
});

export default memo(Header);
