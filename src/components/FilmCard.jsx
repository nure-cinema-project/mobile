import * as React from "react";
import { Text, View } from "react-native";
import Button from "../components/Button";

export default function FilmCard({ film, navigation }) {
  return (
    <View
      style={{
        backgroundColor: "lightgrey",
        borderRadius: 15,
        height: "100%",
        padding: "10%",
      }}
    >
      <Text style={{ fontSize: 30 }}>{film.name}</Text>
      <Text>{film.description}</Text>
      <Button
        mode="contained"
        onPress={() => {
          navigation.navigate("order", film);
        }}
      >
        See screenings
      </Button>
    </View>
  );
}
