import * as React from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

const AuthContext = React.createContext({});

const AuthProvider = ({ children }) => {
  const [credentials, setCredentials] = React.useState({});
  const [authenticated, setAuthenticated] = React.useState(false);

  const loadCredentials = async () => {
    try {
      const credsJson = await AsyncStorage.getItem("credentials");
      const creds = JSON.parse(credsJson || {});
      setCredentials(creds);
      const authJson = await AsyncStorage.getItem("authenticated");
      const auth = JSON.parse(authJson || false);
      setAuthenticated(auth);
    } catch (err) {
      setCredentials({});
    }
  };

  const storeCredentials = async (creds) => {
    try {
      await AsyncStorage.setItem("credentials", JSON.stringify(creds));
      setCredentials(creds);
      await AsyncStorage.setItem("authenticated", JSON.stringify(true));
      setAuthenticated(true);
    } catch (error) {
      Promise.reject(error);
    }
  };

  const deleteCredentials = async () => {
    try {
      await AsyncStorage.setItem("authenticated", JSON.stringify(false));
      setAuthenticated(false);
      await AsyncStorage.setItem("credentials", JSON.stringify({}));
      setCredentials({});
    } catch (error) {
      Promise.reject(error);
    }
  };

  React.useEffect(() => {
    loadCredentials();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        credentials,
        authenticated,
        storeCredentials,
        deleteCredentials,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthProvider };
