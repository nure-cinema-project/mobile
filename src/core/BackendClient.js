import * as React from "react";
import axios from "axios";

const BackendClientContext = React.createContext({});

const host = "http://10.0.2.2:8080";
const api = axios.create({
  baseURL: host + "/api/v1",
  timeout: 1000,
});

const BackendClientProvider = ({ children }) => {
  const fetchFilms = async () => {
    return api.get("/films");
  };
  const fetchScreeningByFilmId = async (id) => {
    return api.get("/screenings", { params: { filmId: id } });
  };
  const fetchTickets = async () => {
    return api.get("/user/tickets");
  };
  const orderSeats = async (screeningId, seatIds) => {
    return api.post("/user/orders", {
      screeningId: screeningId,
      seatIds: seatIds,
    });
  };
  const login = async (email, password) => {
    const request = api.post("/auth/login", {
      email: email,
      password: password,
    });

    request.then((response) => {
      const token = response.data.token;
      api.defaults.headers.common["Authorization"] = token;
    });

    return request;
  };
  const logout = async () => {
    const request = api.post("/auth/logout");

    request.then(
      (response) => delete api.defaults.headers.common["Authorization"]
    );

    return request;
  };
  const register = async (username, email, password) => {
    return api.post("/auth/register", {
      email: email,
      username: username,
      password: password,
    });
  };
  const setAuthorizationHeader = (token) => {
    api.defaults.headers.common["Authorization"] = token;
  };
  const getAuthorizationHeader = (token) => {
    return api.defaults.headers.common["Authorization"];
  };
  const removeAuthorizationHeader = () => {
    delete api.defaults.headers.common["Authorization"];
  };

  return (
    <BackendClientContext.Provider
      value={{
        fetchFilms,
        fetchScreeningByFilmId,
        fetchTickets,
        orderSeats,
        login,
        logout,
        register,
        setAuthorizationHeader,
        getAuthorizationHeader,
        removeAuthorizationHeader,
      }}
    >
      {children}
    </BackendClientContext.Provider>
  );
};

export { BackendClientContext, BackendClientProvider };
