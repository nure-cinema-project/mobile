import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import FilmCarouselScreen from "../screens/FilmCarouselScreen";
import OrderScreen from "../screens/OrderScreen";
import { BackendClientContext } from "../core/BackendClient";
import { ActivityIndicator } from "react-native-paper";
import Header from "../components/Header";
import ContentContainer from "../components/ContentContainer";
import { View } from "react-native";

const Stack = createStackNavigator();

export default function AfficheRoute() {
  const { fetchFilms } = React.useContext(BackendClientContext);
  const [films, setFilms] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);
  const [error, setError] = React.useState(false);

  React.useEffect(() => {
    fetchFilms().then(
      (response) => {
        const films = response.data;
        setFilms(films);
        setIsLoading(false);
      },
      (reason) => {
        console.log(reason);
        setError(true);
      }
    );
  }, [setFilms]);

  return (
    <NavigationContainer>
      {isLoading ? (
        <ContentContainer>
          {!error ? (
            <View>
              <Header>Loading...</Header>
              <ActivityIndicator size="large"></ActivityIndicator>
            </View>
          ) : (
            <Header>Error while loading!</Header>
          )}
        </ContentContainer>
      ) : (
        <Stack.Navigator
          initialRouteName="affiche"
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen
            name="affiche"
            component={FilmCarouselScreen}
            initialParams={{ films: films }}
          />
          <Stack.Screen name="order" component={OrderScreen} />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
}
