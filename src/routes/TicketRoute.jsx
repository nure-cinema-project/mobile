import * as React from "react";
import { RefreshControl, ScrollView, View } from "react-native";
import {
  ActivityIndicator,
  Button,
  Subheading,
  Text,
} from "react-native-paper";
import ContentContainer from "../components/ContentContainer";
import Header from "../components/Header";
import TicketCard from "../components/TicketCard";
import { AuthContext } from "../core/AuthenticationManager";
import { BackendClientContext } from "../core/BackendClient";

export default function TicketRoute({ jumpTo }) {
  const { authenticated, credentials } = React.useContext(AuthContext);
  const { fetchTickets } = React.useContext(BackendClientContext);
  const [tickets, setTickets] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);
  const [error, setError] = React.useState(false);
  const [refreshing, setRefreshing] = React.useState(false);

  React.useEffect(() => {
    if (authenticated) {
      fetchTickets().then(
        (response) => {
          const tickets = response.data;
          setTickets(tickets);
          setIsLoading(false);
        },
        (reason) => {
          console.log(reason);
          setError(true);
        }
      );
    }
  }, [setTickets, authenticated, credentials.email]);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    fetchTickets().then(
      (response) => {
        const tickets = response.data;
        setTickets(tickets);
        setRefreshing(false);
      },
      (reason) => {
        console.log(reason);
        setError(true);
        setRefreshing(false);
      }
    );
  }, []);

  return (
    <View
      style={{
        flex: 1,
        flexDirection: "column",
        paddingTop: "8%",
      }}
    >
      <View
        style={{
          width: "100%",
          alignItems: "center",
        }}
      >
        <Header>My tickets</Header>
        {error && <Header>Error while loading!</Header>}
      </View>
      {authenticated ? (
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          {isLoading && <ActivityIndicator size="large"></ActivityIndicator>}
          {tickets.map((t) => (
            <TicketCard key={t.number} ticket={t} />
          ))}
        </ScrollView>
      ) : (
        <ContentContainer>
          <Subheading>Please login to see your tickets!</Subheading>
          <Button
            mode="contained"
            onPress={() => {
              jumpTo("profile");
            }}
          >
            Login
          </Button>
        </ContentContainer>
      )}
    </View>
  );
}
