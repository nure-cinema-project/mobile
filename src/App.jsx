import * as React from "react";
import { BottomNavigation } from "react-native-paper";
import { AuthContext, AuthProvider } from "./core/AuthenticationManager";
import {
  BackendClientContext,
  BackendClientProvider,
} from "./core/BackendClient";
import AfficheRoute from "./routes/AfficheRoute";
import ProfileRoute from "./routes/ProfileRoute";
import TicketRoute from "./routes/TicketRoute";

export default function App() {
  return (
    <AuthProvider>
      <BackendClientProvider>
        <InnterApplication />
      </BackendClientProvider>
    </AuthProvider>
  );
}

function InnterApplication() {
  const [index, setIndex] = React.useState(0);
  const { setAuthorizationHeader, getAuthorizationHeader } =
    React.useContext(BackendClientContext);
  const { authenticated, credentials } = React.useContext(AuthContext);

  if (authenticated && getAuthorizationHeader() == null) {
    setAuthorizationHeader(credentials.token);
  }

  const [routes] = React.useState([
    { key: "profile", title: "Profile", icon: "account-circle" },
    { key: "affiche", title: "Affiche", icon: "movie" },
    { key: "tickets", title: "My Tickets", icon: "ticket" },
  ]);

  const renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case "profile":
        return <ProfileRoute jumpTo={jumpTo} />;
      case "affiche":
        return <AfficheRoute jumpTo={jumpTo} />;
      case "tickets":
        return <TicketRoute jumpTo={jumpTo} />;
    }
  };

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  );
}
