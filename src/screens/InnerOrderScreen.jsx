import * as React from "react";
import { View } from "react-native";
import { IconButton, Text, ToggleButton, Snackbar } from "react-native-paper";
import Button from "../components/Button";
import ContentContainer from "../components/ContentContainer";
import Header from "../components/Header";
import Seat from "../components/Seat";
import { BackendClientContext } from "../core/BackendClient";

export default function InnerOrderScreen({ screenings, filmName, navigation }) {
  if (screenings.length !== 0) {
  }
  const [screeningIndex, setScreeningIndex] = React.useState(0);
  const [clientReservation, setClientReservation] = React.useState([]);
  const hallName = screenings[screeningIndex].hall.name;
  const screeningId = screenings[screeningIndex].id;

  const times = screenings.map((s, index) => {
    return (
      <ToggleButton
        style={{ width: 200, borderWidth: 1, margin: 3 }}
        icon={() => (
          <Text style={{ color: s.hall.name.toLowerCase() }}>
            {s.startTime} | {s.hall.name}
          </Text>
        )}
        value={index}
        key={index}
      />
    );
  });

  return (
    <ContentContainer>
      <IconButton
        icon="arrow-left"
        style={{
          position: "absolute",
          top: "8%",
          left: "-5%",
        }}
        size={30}
        onPress={() => navigation.goBack()}
      />
      <Header>{filmName}</Header>
      <ToggleButton.Group
        style={{ padding: 20, backgroundColor: "rose" }}
        onValueChange={(screeningIndex) => {
          if (screeningIndex !== null) {
            setScreeningIndex(screeningIndex);
            setClientReservation([]);
          }
        }}
        value={screeningIndex}
      >
        {times}
      </ToggleButton.Group>
      <Text>Ticket Price: {screenings[screeningIndex].price}₴</Text>

      <Header
        style={{
          color: hallName.toLowerCase(),
        }}
      >
        {hallName} Hall
      </Header>
      <View
        style={{
          width: "100%",
          borderRadius: 2,
          height: 12,
          backgroundColor: "gray",
          marginBottom: 24,
        }}
      />
      <ScreeningSeats
        hallName={screenings[screeningIndex].hall.name}
        seats={screenings[screeningIndex].hall.seats}
        reservations={screenings[screeningIndex].reservations}
        setClientReservation={setClientReservation}
        clientReservation={clientReservation}
        screeningId={screeningId}
      />
    </ContentContainer>
  );
}

function ScreeningSeats({
  seats,
  reservations,
  hallName,
  setClientReservation,
  clientReservation,
  screeningId,
}) {
  const { orderSeats } = React.useContext(BackendClientContext);
  const [visible, setVisible] = React.useState(false);
  const [visibleError, setVisibleError] = React.useState(false);

  const seatsSeatCount = Math.max.apply(
    Math,
    seats.map((s) => s.seat)
  );

  const seatsJsx = seats.map((s, index) => {
    const key = screeningId.toString() + s.id.toString();
    return (
      <View
        key={key}
        style={{
          flexBasis: 100.0 / seatsSeatCount + "%",
          padding: 5,
          alignContent: "center",
          alignItems: "center",
        }}
      >
        <Seat
          key={key}
          line={s.line}
          seat={s.seat}
          pickColor={hallName.toLowerCase()}
          text={s.line.toString() + s.seat.toString()}
          disabled={reservations.find((r) => r.seat.id == s.id) != null}
          onPressDone={(line, seat, pressed) => {
            if (pressed) {
              setClientReservation((prevState) => [...prevState, s]);
            } else {
              const newArray = clientReservation.filter(
                (item) => item.id !== s.id
              );
              setClientReservation(newArray);
            }
          }}
        />
      </View>
    );
  });

  const clientReservationJsx = clientReservation.map((r, index) => (
    <Text key={index}>
      Ticket {r.line}
      {r.seat}
    </Text>
  ));

  const onOrderButtonPressed = React.useCallback(() => {
    if (clientReservation.length !== 0) {
      clientReservationIds = clientReservation.map((seat) => seat.id);
      orderSeats(screeningId, clientReservationIds).then(
        (response) => {
          clientReservation.forEach((c) => {
            reservations.push({ id: -1, screeningId: screeningId, seat: c });
          });
          setVisible(true);
        },
        (reason) => {
          console.log(reason);
          setVisibleError(true);
        }
      );

      setClientReservation([]);
    }
  });

  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        width: "100%",
      }}
    >
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "center",
          margin: -5,
        }}
      >
        {seatsJsx}
      </View>
      <View>{clientReservationJsx}</View>
      <Button
        mode="contained"
        onPress={() => {
          onOrderButtonPressed();
        }}
      >
        Order
      </Button>
      <Snackbar
        visible={visible}
        action={{
          label: "OK",
          onPress: () => {
            setVisible(false);
          },
        }}
        onDismiss={() => {
          setVisible(false);
        }}
      >
        Thank you for your order!
      </Snackbar>
      <Snackbar
        visible={visibleError}
        action={{
          label: "OK",
          onPress: () => {
            setVisibleError(false);
          },
        }}
        onDismiss={() => {
          setVisibleError(false);
        }}
      >
        Error! Please try later.
      </Snackbar>
    </View>
  );
}
