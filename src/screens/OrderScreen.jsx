import * as React from "react";
import { View } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import ContentContainer from "../components/ContentContainer";
import Header from "../components/Header";
import { BackendClientContext } from "../core/BackendClient";
import InnerOrderScreen from "./InnerOrderScreen";

export default function OrderScreen({ route, navigation }) {
  const film = route.params;
  const filmName = film.name;
  const { fetchScreeningByFilmId, orderSeats } =
    React.useContext(BackendClientContext);
  const [screenings, setScreenings] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);
  const [error, setError] = React.useState(false);

  React.useEffect(() => {
    fetchScreeningByFilmId(film.id).then(
      (response) => {
        const screenings = response.data;
        setScreenings(screenings);
        setIsLoading(false);
      },
      (reason) => {
        console.log(reason);
        setError(true);
      }
    );
  }, [setScreenings]);

  const body = isLoading ? (
    <ContentContainer>
      {!error ? (
        <View>
          <Header>Loading...</Header>
          <ActivityIndicator size="large"></ActivityIndicator>
        </View>
      ) : (
        <Header>Error while loading!</Header>
      )}
    </ContentContainer>
  ) : (
    <InnerOrderScreen
      screenings={screenings}
      filmName={filmName}
      navigation={navigation}
    />
  );
  return body;
}
