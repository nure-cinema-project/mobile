import * as React from "react";
import { View } from "react-native";
import styles, { itemWidth, sliderWidth } from "../styles/Carousel.style";

import Carousel, { Pagination } from "react-native-snap-carousel";
import FilmCard from "../components/FilmCard";

export default function FilmCarouselScreen(props) {
  const [carousel, setCarousel] = React.useState({});
  const [activeIndex, setActiveIndex] = React.useState(0);

  const _renderItem = ({ item, index }) => {
    return <FilmCard film={item} navigation={props.navigation} />;
  };

  return (
    <View style={styles.carouselContainer}>
      <Carousel
        layout={"default"}
        ref={(ref) => setCarousel(ref)}
        data={props.route.params.films}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        renderItem={_renderItem}
        onSnapToItem={(index) => setActiveIndex(index)}
      />
      <Pagination
        dotsLength={props.route.params.films.length}
        activeDotIndex={activeIndex}
        dotStyle={styles.paaginationDot}
        carouselRef={carousel}
        tappableDots={!!carousel}
      />
    </View>
  );
}
