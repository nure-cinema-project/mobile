import * as React from "react";
import ContentContainer from "../components/ContentContainer";
import Header from "../components/Header";
import Button from "../components/Button";
import { AuthContext } from "../core/AuthenticationManager";
import { BackendClientContext } from "../core/BackendClient";

export default function ProfileScreen() {
  const { credentials, deleteCredentials } = React.useContext(AuthContext);
  const { logout } = React.useContext(BackendClientContext);

  return (
    <ContentContainer>
      <Header>Hi, {credentials.username}!</Header>

      <Button
        mode="contained"
        onPress={() => {
          logout();
          deleteCredentials();
        }}
      >
        Logout
      </Button>
    </ContentContainer>
  );
}
